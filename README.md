# Noita-mimics-mod

## List of ideas for mimimics
1. Gold nuggets
1. Potion pedestals
1. Wand pedestals
1. Wands
1. Reroll machines
1. Temple (HM) Statues
1. Tablets
1. Books
1. Barrels
1. Brewing stands
1. Wheelcart
1. Crate
1. Music machine
1. Bones
1. Vases
1. Cursors
1. Health bar
1. HM Shop sale indicator

Possible:
1. Furniture (Beds, wardrobes)

## Possible mimic visual
1. Teeth / mouth -- similar to chest/heart mimics
2. Spikes that protrude from the edges of the sprite

## Sprites stats
Smallest width: 
1. health_slider_front.png -- 3px
2. goldnugget_01.png -- 6px
3. goldnugget_9px.png -- 9px
4. material_pouch.png -- 9px
5. gourd.png -- 10px

Biggest width:
mouse_cursor_big_system.png -- 51px

Smallest height:
1. health_slider_front.png -- 3px
2. goldnugget_01.png -- 6px
3. goldnugget_9px.png -- 9px
4. gourd.png -- 10px

Biggest height:
mouse_cursor_big_system.png -- 51px